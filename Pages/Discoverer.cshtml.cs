using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Its_Spacyy.Models;
using Its_Spacyy.Facades;
using System;

namespace Its_Spacyy.Pages
{
	public class DiscovererModel : PageModel
	{
		private readonly ILogger<CelestialBodyModel> _logger;
		public Discoverer TheOne {get; private set; }


		public DiscovererModel(ILogger<CelestialBodyModel> logger) {
			_logger = logger;
		}

		public IActionResult OnGet(string name) {
			TheOne = SparqlFacade.GetDiscoverer(name);
			return Page();
		}
	}
}
