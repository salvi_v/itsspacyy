using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Its_Spacyy.Models;
using Its_Spacyy.Facades;

namespace Its_Spacyy.Pages {
	public class CelestStarModel : PageModel {
		private readonly ILogger<CelestialBodyModel> _logger;
		public List<CelestialBody> CelestialBodies {get; private set; }
		public List<CelestialBody> TodayStars {get; private set; }

		public CelestStarModel(ILogger<CelestialBodyModel> logger) {
			_logger = logger;
		}

		public IActionResult OnGet() {
			TodayStars = SparqlFacade.GetStars();
			return Page();
		}
	}
}
