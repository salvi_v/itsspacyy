using System.Collections.Generic;
using System;
using VDS.RDF.Query;
using Its_Spacyy.Models;
using System.Linq;

namespace Its_Spacyy.Facades {
	public class SparqlFacade {
		public static int i=-1;
		
		//requête pour avoir les celestial bodies
		public static SparqlResultSet GetRequest() {
			SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://dbpedia.org/sparql"), "http://dbpedia.org");
			return endpoint.QueryWithResultSet(@"
				select * where {
					?nom rdf:type dbo:CelestialBody.
					?nom rdfs:label ?label.
					?nom dbo:discovered ?discovered.
					?nom dbo:absoluteMagnitude ?absoluteMagnitude.
					?nom dbo:thumbnail ?picture.
					?nom dbo:apoapsis ?apoapsis.
					?nom dbo:periapsis ?periapsis.
					?nom dbo:discoverer ?discoverer.
					?nom dbo:abstract ?abstract
					filter(langMatches(lang(?abstract),""en""))
					filter(langMatches(lang(?label),""en""))
				}
			");
		}

		// crée les celestial bodies
		public static List<CelestialBody> GetBody() {
			SparqlResultSet res = GetRequest();
			List<CelestialBody> cb = new List<CelestialBody>();
			foreach (var celest in res){
				cb.Add(new CelestialBody(celest));
			}
			return cb;
		}

		// crée les trois celestial bodies du jour
		public static List<CelestialBody> GetStars() {
			List<CelestialBody> TodayStars = new List<CelestialBody>();
			int i = 0;
			string date = DateTime.Now.ToString();
			//ajoute des 0 aux jours et mois < 10
			if (date.IndexOf("/")==1){
				date = "0"+date;
			}
			if (date.IndexOf("/",3)==4) {
				date = date.Remove(3)+"0"+date.Substring(3);
			}
			SparqlResultSet res = GetRequest();
			//on récupère uniquement les astres découverts le même jour/mois que aujourd'hui, si pas assez on regardes le même mois
			foreach(var celest in res) {
				string body = GetAttribut(celest, "discovered");
				// date = 01/02/2018 => date.Remove(2) = 01 ET date.Remove(5).Substring(3) = 02
				if( string.Compare(body.Substring(8), date.Remove(2)) == 0 & string.Compare(body.Remove(7).Substring(5), date.Remove(5).Substring(3))==0) {
					TodayStars.Add(new CelestialBody(celest));
					i = i+1;
				}
				if(i==3) break;
			}
			if(i<3) {
				foreach(var celest in res) {
					string body = GetAttribut(celest, "discovered");
					if (string.Compare(body.Remove(7).Substring(5), date.Remove(5).Substring(3))==0) {
						TodayStars.Add(new CelestialBody(celest));
						i = i+1;
					}
					if(i==3) break;
				}
			}
			return TodayStars;
		}

		//crée le discoverer
		public static Discoverer GetDiscoverer(string name) {
			SparqlResultSet res = GetDiscoRequest(name);
			Discoverer disc = new Discoverer(res.First(), name.Replace("_"," "));
			return disc;
		}


		public static SparqlResultSet GetDiscoRequest(string name) {
			SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("https://dbpedia.org/sparql"), "http://dbpedia.org");
			string query = @"
				select * where {
					?nom rdf:type dbo:CelestialBody.
					?nom dbo:discoverer ?discoverer.
					?nom rdfs:label ?labelBody.
					?discoverer rdfs:label ?label.
					?discoverer dbo:abstract ?abstract.
					?discoverer dbo:birthDate ?birthDate.
					?discoverer dbo:birthPlace ?birthPlace.
					optional {?discoverer dbo:deathDate ?deathDate}.
					filter(langMatches(lang(?abstract),""en""))
					filter(langMatches(lang(?label),""en""))
					filter(?labelBody = """;
			name = name.Replace("_"," ");
			query = query + name;
			query = query + @"""@en)
			}";
			return endpoint.QueryWithResultSet(query);
		}

		//remplis les infos sur les attributs de chaque celestial body
		public static string GetAttribut(SparqlResult res, string attr) {
			if (res.HasBoundValue(attr)) {
				if(attr=="label") return res.Value(attr).ToString().Replace("@en","");
				else if (attr=="abstract") return res.Value(attr).ToString().Replace("@en","");
				else if (attr=="apoapsis") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#double","");
				else if (attr=="periapsis") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#double","");
				else if (attr=="absoluteMagnitude") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#double","");
				else if (attr=="discovered") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#date", "").Replace("-","/");
				else if (attr=="picture") return res.Value(attr).ToString();
				else if (attr=="birthDate") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#date", "").Replace("-","/");
				else if (attr=="birthPlace") {
					string s = res.Value(attr).ToString();
					int i = s.LastIndexOf("/");
					return s.Substring(i+1);
				} else if (attr=="deathDate") return res.Value(attr).ToString().Replace("^^http://www.w3.org/2001/XMLSchema#date", "").Replace("-","/");
				else if (attr=="discoverer") {
					string s = res.Value(attr).ToString().Replace("@en","").Replace("_"," ");
					int i = s.LastIndexOf("/");
					return s.Substring(i+1);
				} else {
					Console.WriteLine("Attribut Error : no "+attr);
					return "Error";
				}
			} else {
				if (attr=="discovererLink") return res.Value("discoverer").ToString().Replace("@en","");
				else {
					Console.WriteLine("Attribut Error : no "+attr);
					return "Error";
				}
			}
		}

		public static List<CelestialBody> GetBodies(List<CelestialBody> list, string name) {
			List<CelestialBody> newList = new List<CelestialBody>();
			foreach(CelestialBody cel in list) {
				if(string.Compare(cel.Discoverer, name) == 0) {
					newList.Add(cel);
				}
			}
			return newList;
		}
	}
}
