
using Its_Spacyy.Facades;
using VDS.RDF.Query;

namespace Its_Spacyy.Models {
	public class CelestialBody {
		public string Name {get; set; }
		public string AbsoluteMagnitude {get; set; }
		public string Apoapsis {get; set; }
		public string Periapsis {get; set; }
		public string Discovered {get; set; }
		public string DiscovererLink {get;set; }
		public string Discoverer {get; set; }
		public string Abstract {get; set; }
		public string Picture {get; set; }

		public CelestialBody(SparqlResult res) {
			Name = SparqlFacade.GetAttribut(res, "label");
			AbsoluteMagnitude = SparqlFacade.GetAttribut(res, "absoluteMagnitude");
			Apoapsis = SparqlFacade.GetAttribut(res, "apoapsis");
			Periapsis = SparqlFacade.GetAttribut(res, "periapsis");
			Discovered = SparqlFacade.GetAttribut(res, "discovered");
			Discoverer = SparqlFacade.GetAttribut(res, "discoverer");
			DiscovererLink = SparqlFacade.GetAttribut(res, "discovererLink");
			Abstract = SparqlFacade.GetAttribut(res, "abstract");
			Picture = SparqlFacade.GetAttribut(res, "picture");
		}
	}
}
