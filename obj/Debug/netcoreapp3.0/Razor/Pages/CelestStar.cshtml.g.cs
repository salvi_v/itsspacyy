#pragma checksum "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "13fae57b02fb240b52e424aac31f483a3410fd5d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Its_Spacyy.Pages.Pages_CelestStar), @"mvc.1.0.razor-page", @"/Pages/CelestStar.cshtml")]
namespace Its_Spacyy.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/_ViewImports.cshtml"
using Its_Spacyy;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
using Its_Spacyy.Pages;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"13fae57b02fb240b52e424aac31f483a3410fd5d", @"/Pages/CelestStar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1a25ca663a28480d035bd3b3bc31ba0725c01d72", @"/Pages/_ViewImports.cshtml")]
    public class Pages_CelestStar : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("navbar-brand"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "/Discoverer", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
  
  ViewData["Title"] = "Stars of the day";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div align=center>\r\n\t<h3 style=\"background-color:blue; border: 5px dashed #1C6EA4; border-radius: 40px;\">\r\n\t\tOur TOP 3 Celestial Bodies of the day !\r\n\t</h3>\r\n</div>\r\n<br>\r\n\r\n");
#nullable restore
#line 15 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
  
	int i=0;
	foreach (var item in Model.TodayStars) {
		i++;
		if (i%2==0) {

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-3\">\r\n\t\t\t\t\t<img class=\"img-fluid d-felx\"");
            BeginWriteAttribute("src", " src=\"", 483, "\"", 502, 1);
#nullable restore
#line 22 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
WriteAttributeValue("", 489, item.Picture, 489, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"Image non disponible\"/>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-7\">\r\n\t\t\t\t\t<h2>");
#nullable restore
#line 25 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                   Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2><h6>Discovered on ");
#nullable restore
#line 25 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                    Write(item.Discovered);

#line default
#line hidden
#nullable disable
            WriteLiteral(" by ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "13fae57b02fb240b52e424aac31f483a3410fd5d5621", async() => {
#nullable restore
#line 25 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                                                                                                                                  Write(item.Discoverer);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-name", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 25 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                                                                                               WriteLiteral(item.Name.Replace(" ","_"));

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["name"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-name", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["name"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</h6>\r\n\t\t\t\t\t<br> <h5>Description :</h5>\r\n\t\t\t\t\t<p>");
#nullable restore
#line 27 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                  Write(item.Abstract);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-2\">\r\n\t\t\t\t\t<br> <h5>More informations :</h5>\r\n\t\t\t\t\t<br> <h6>Absolute Magnitude : </h6> ");
#nullable restore
#line 31 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                   Write(item.AbsoluteMagnitude);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t\t<br> <h6>Apoapsis : </h6> ");
#nullable restore
#line 32 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                         Write(item.Apoapsis);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t\t<br> <h6>Periapsis : </h6> $");
#nullable restore
#line 33 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                           Write(item.Periapsis);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n");
#nullable restore
#line 36 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
		} else {

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-7\">\r\n\t\t\t\t\t<h2>");
#nullable restore
#line 39 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                   Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2><h6>Discovered on ");
#nullable restore
#line 39 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                    Write(item.Discovered);

#line default
#line hidden
#nullable disable
            WriteLiteral(" by ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "13fae57b02fb240b52e424aac31f483a3410fd5d10593", async() => {
#nullable restore
#line 39 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                                                                                                                                  Write(item.Discoverer);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-name", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 39 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                                                                                                               WriteLiteral(item.Name.Replace(" ","_"));

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["name"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-name", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["name"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</h6>\r\n\t\t\t\t\t<br> <h5>Description :</h5>\r\n\t\t\t\t\t<p>");
#nullable restore
#line 41 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                  Write(item.Abstract);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-2\">\r\n\t\t\t\t\t<br> <h5>More informations :</h5>\r\n\t\t\t\t\t<p>Absolute Magnitude : ");
#nullable restore
#line 45 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                                       Write(item.AbsoluteMagnitude);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t\t<p>Apoapsis : ");
#nullable restore
#line 46 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                             Write(item.Apoapsis);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t\t<p>Periapsis : ");
#nullable restore
#line 47 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
                              Write(item.Periapsis);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-3\">\r\n\t\t\t\t\t<img class=\"img-fluid\"");
            BeginWriteAttribute("src", " src=\"", 1661, "\"", 1680, 1);
#nullable restore
#line 50 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
WriteAttributeValue("", 1667, item.Picture, 1667, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"Image non disponible\"/>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n");
#nullable restore
#line 53 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
		}

#line default
#line hidden
#nullable disable
            WriteLiteral("\t\t<br><br><br><br>\r\n");
#nullable restore
#line 55 "/Users/salviverma/Dropbox/cours cnam/fip2/SIW/projet/projet 9 en cours/Its_Spacyy/Pages/CelestStar.cshtml"
	}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Its_Spacyy.Pages.CelestStarModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Its_Spacyy.Pages.CelestStarModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Its_Spacyy.Pages.CelestStarModel>)PageContext?.ViewData;
        public Its_Spacyy.Pages.CelestStarModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
