using Its_Spacyy.Facades;
using VDS.RDF.Query;
using System.Collections.Generic;
using System;

namespace Its_Spacyy.Models {
  public class Discoverer {
    public string Name {get; set; }
		public List<CelestialBody> Bodies {get; set; }
		public string Abstract {get; set; }
		public string BirthDate {get; set; }
		public string BirthPlace {get; set; }
		public string DeathDate {get; set; }

		public Discoverer(SparqlResult res, string name) {
			Name = SparqlFacade.GetAttribut(res, "label");
			Bodies = SparqlFacade.GetBodies(SparqlFacade.GetBody(), Name);
			Abstract = SparqlFacade.GetAttribut(res, "abstract");
			BirthDate = SparqlFacade.GetAttribut(res, "birthDate");
			BirthPlace = SparqlFacade.GetAttribut(res, "birthPlace");
			DeathDate = SparqlFacade.GetAttribut(res, "deathDate");
		}
	}
}